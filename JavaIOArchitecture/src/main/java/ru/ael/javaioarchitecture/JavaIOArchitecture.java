/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.ael.javaioarchitecture;

import ru.ael.javaioarchitecture.consoleio.TeminalLoginManager;

/**
 *
 * @author developer
 */
public class JavaIOArchitecture {

    // С метода main начинается выполнение программы
    // 
    //  Создание экземпляров классов, придуманных программистом
    
    public static void main(String[] args) {

        TeminalLoginManager telminal = new TeminalLoginManager();
        telminal.start();
        System.out.println("[main] login: "+telminal.getLogin());
        System.out.println("[main] password: "+telminal.getPassword());
        
        
        
        
    }
}
