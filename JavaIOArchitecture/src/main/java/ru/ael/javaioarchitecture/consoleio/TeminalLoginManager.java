/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.javaioarchitecture.consoleio;

import java.util.Scanner;

/**
 *  Получение логина и пароля в ходе ввода с терминала
 * 
 * 
 * @author developer
 */
public class TeminalLoginManager {

    private String login;
    private String password;

    public void start() {
        // System.in - ожидание символов с клавиатуры
        Scanner scan = new Scanner(System.in); // Входной поток от пользователя ПК
        System.out.print("Логин для входа в кабинет студента > ");
        login = scan.nextLine();  // Получение следующей строки         
        System.out.print("Введите пароль: ");
        password = scan.nextLine();
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}
